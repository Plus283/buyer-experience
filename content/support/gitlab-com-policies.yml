---
title: GitLab.com Specific Support Policies
description: GitLab.com Specific Support Policies
support-hero:
  data:
    title: GitLab.com Specific Support Policies
side_menu:
  anchors:
    text: "ON THIS PAGE"
    data:
      - text: "Account Recovery and 2FA Resets"
        href: "#account-recovery-and-2fa-resets"                    
      - text: "Restoration of Deleted Data"
        href: "#restoration-of-deleted-data"
      - text: "Ownership Disputes"
        href: "#ownership-disputes"
      - text: "Name Squatting Policy"
        href: "#name-squatting-policy"
      - text: "Namespace & Trademarks"
        href: "#namespace--trademarks"
      - text: "Log requests"
        href: "#log-requests"                                                                                                                                                    
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy
  data:
    block:
    - subtitle:
        text: Account Recovery and 2FA Resets
        id: account-recovery-and-2fa-resets
      text: |
        <p>
          If you are unable to sign into your account we can help you regain access. 
          <strong>This service is only available for paid users</strong> (you are part of paid group or paid user namespace).
        </p>
        <br />
        <p><strong>Forgotten password?</strong></p>
        <ol>
          <li>Use the <a href="https://gitlab.com/users/password/new">Reset password form</a></li>
          <li>If you don't receive the reset email please <a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000803379">contact support</a></li>
        </ol>
        <p><strong>Locked out by 2FA?</strong></p>
        <br />
        <p>Are you on a free plan? <a href="/blog/2020/08/04/gitlab-support-no-longer-processing-mfa-resets-for-free-users/">Please read this blog post</a></p>
        <ol>
          <li>Try to <a href="https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#generate-new-recovery-codes-using-ssh">generate new recovery keys using SSH</a></li>
          <li>
            Paid users only: If you are still unable to sign in after trying step one, <a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000803379">contact support</a>. 
            We will request evidence to prove account ownership. We cannot guarantee to recover your account unless you pass the verification checks.
          </li>
        </ol>
        <p>
          Please note that in some cases reclaiming an account may be impossible. 
          Read <a href="/blog/2018/08/09/keeping-your-account-safe/">"How to keep your GitLab account safe"</a>
          for advice on preventing this.
        </p>
        <br />
        <p><strong>Account blocked?</strong></p>
        <br />
        <p>
          If your account has been blocked, please <a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000803379">contact support</a>.
        </p>
    - subtitle: 
        text: Restoration of Deleted Data
        id: restoration-of-deleted-data
      text: |
        <p>
          Any type of data restoration is currently a manual and time consuming process lead by GitLab’s infrastructure team. 
          <a href="/handbook/engineering/infrastructure/faq/#q-if-a-customer-project-is-deleted-can-it-be-restored">Our infrastructure team clearly states</a> that “once a project is deleted it cannot be restored”.
        </p>
        <br />
        <p>We encourage customers to use:</p>
        <ol>
          <li>the <a href="https://docs.gitlab.com/ee/user/group/#enabling-delayed-project-removal">delayed deletion</a> feature if it is available in your plan,</li>
          <li><a href="https://docs.gitlab.com/ee/user/project/settings/import_export.html">export projects and/or groups</a> regularly, particularly via the API.</li>
        </ol>
        <p>
          GitLab will consider restoration requests only when the request is for a project or group that is part of a <strong>paid plan</strong> 
          with an active subscription applied, and one of the following is true:
        </p>
        <ul>
          <li>The data was deleted due to a GitLab bug.</li>
          <li>The organization’s contract includes a specific provision.</li>
        </ul>
        <p>Please note that user accounts and individual contributions cannot be restored.</p>
    - subtitle:
        text: Ownership Disputes
        id: ownership-disputes
      text: |
        <p>
          GitLab will not act as an arbitrator of Group or Account ownership disputes. 
          Each user and group owner is responsible for ensuring that they are following best 
          practices for data security.
        </p>
        <br />
        <p>
          As GitLab subscriptions are generally business-to-business transactions, in the event that a former
           employee has revoked company access to a paid group, please contact GitLab Support for recovery options.
        </p>
    - subtitle:
        text: Name Squatting Policy
        id: name-squatting-policy
      text: |
        <p>Per the <a href="https://about.gitlab.com/terms/">GitLab Terms of Service</a>:</p>
        <br />
        <blockquote>
          <p>
            Account name squatting is prohibited by GitLab. Account names on GitLab are administered to users on a first-come, 
            first-serve basis. Accordingly, account names cannot be held or remain inactive for future use.
          </p>
        </blockquote>
        <br />
        <p>
          The GitLab.com Support Team will consider a <a href="https://docs.gitlab.com/ee/user/group/#namespaces">namespace</a> (user name or group name) to fall under the provisions of this policy
          when the user has not logged in or otherwise used the namespace for an extended time.
        </p>
        <br />
        <p>
          Namespaces will be released, if eligible under the criteria below, upon request by a member of a paid namespace or sales approved prospect.
        </p>
        <br />
        <p>Specifically:</p>
        <ul>
          <li>
            User namespaces can be reassigned if both of the following are true:
            <ol>
              <li>The user's last sign in was at least two years ago.</li>
              <li>The user is not the sole owner of any active projects.</li>
            </ol>
          </li>
          <li>
            Group namespaces can be reassigned if one of the following is true:
            <ol>
              <li>There is no data (no project or project(s) are empty).</li>
              <li>The owner's last sign in was at least two years ago.</li>
            </ol>
          </li>
        </ul>
        <p>
          If the namespace contains data, GitLab Support will attempt to contact the owner over a two week period before reassigning the namespaces. 
          If the namespace contains no data (empty or no projects) and the owner is inactive, the namespace will be released immediately.
        </p>
        <br />
        <p>
          Namespaces associated with unconfirmed accounts over 90 days old are eligible for immediate release. 
          Group namespaces that contain no data and were created more than 6 months ago are likewise eligible for immediate release.
        </p>
        <br />
        <p>
          NOTE: The minimum characters required for a namespace is <code>2</code>, it is no longer possible to have a namespace of <code>1</code> character.
        </p>
    - subtitle:
        text: Namespace & Trademarks
        id: namespace--trademarks
      text: |
        <p>
          GitLab.com namespaces are available on a first come, first served basis and cannot be reserved. No brand, company, entity, or persons own the rights 
          to any namespace on GitLab.com and may not claim them based on the trademark. Owning the brand "GreatCompany" does not mean owning the namespace "gitlab.com/GreatCompany". 
          Any dispute regarding namespaces and trademarks must be resolved by the parties involved. GitLab Support will never act as arbitrators or intermediaries in these disputes 
          and will not take any action without the appropriate legal orders.
        </p>
    - subtitle:
        text: Log requests
        id: log-requests
      text: |
        <p>
          Due to our <a href="/terms/">terms</a>, GitLab Support cannot provide raw copies of logs. However, if users have concerns, Support can answer specific questions and provide 
          summarized information related to the content of log files.
        </p>
        <br />
        <p>
          For paid users on GitLab.com, many actions are logged in the <a href="https://docs.gitlab.com/ee/administration/audit_events.html#group-events">Audit events</a> section of your GitLab.com project or group.
        </p>
