---
  title: Secure
  description: Security capabilities, integrated into your development lifecycle with GitLab. Learn more here!
  components:
    - name: sdl-cta
      data:
        title: Secure
        aos_animation: fade-down
        aos_duration: 500
        subtitle: |
          Security capabilities, integrated into your development lifecycle.
        text: |
          GitLab provides Static Application Security Testing (SAST), Dynamic Application Security Testing (DAST), Container Scanning, and Dependency Scanning to help you deliver secure applications along with license compliance.
        icon:
          name: secure-alt-2
          alt: Secure Spotlight Icon
          variant: marketing
    - name: featured-media
      data:
        column_size: 6
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        media:
          - title: SAST
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Static Application Security Testing scans the application source code and binaries to spot potential vulnerabilities before deployment using open source tools that are installed as part of GitLab. Vulnerabilities are shown in-line with every merge request and results are collected and presented as a single report.
            link:
              href: https://docs.gitlab.com/ee/user/application_security/sast/
              text: Learn More
              data_ga_name: sast learn more
              data_ga_location: body
          - title: Secret Detection
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Check for credentials and secrets in commits.
            link:
              href: https://docs.gitlab.com/ee/user/application_security/secret_detection/
              text: Learn More
              data_ga_name: secret detection learn more
              data_ga_location: body
          - title: Code Quality
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Automatically analyze your source code to surface issues and see if quality is improving or getting worse with the latest commit.
            link:
              href: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html
              text: Learn More
              data_ga_name: code quality learn more
              data_ga_location: body
          - title: DAST
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Dynamic Application Security Testing analyzes your running web application for known runtime vulnerabilities. It runs live attacks against a Review App, an externally deployed application, or an active API, created for every merge request as part of the GitLab's CI/CD capabilities. Users can provide HTTP credentials to test private areas. Vulnerabilities are shown in-line with every merge request. Tests can also be run outside of CI/CD pipelines by utilizing on-demand DAST scans.
            link:
              href: https://docs.gitlab.com/ee/user/application_security/dast/
              text: Learn More
              data_ga_name: dast learn more
              data_ga_location: body
          - title: API Security
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              API Security focuses on testing and protecting APIs. Testing for known vulnerabilities with DAST API and unknown vulnerabilities with API Fuzzing, API Security runs against a live API or a *Review App* to discover vulnerabilities that can only be uncovered after the API has been deployed. Users can provide credentials to test authenticated APIs. Vulnerabilities are shown in-line with every merge request.
            link:
              href: https://docs.gitlab.com/ee/user/application_security/dast_api/
              text: Learn More
              data_ga_name: api security learn more
              data_ga_location: body
          - title: Fuzz Testing
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Fuzz testing increase chances to get results by using arbitrary payloads instead of well-known ones.
            link:
              href: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/
              text: Learn More
              data_ga_name: fuzz testing learn more
              data_ga_location: body
          - title: Dependency Scanning
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Analyze external dependencies (e.g. libraries like Ruby gems) for known vulnerabilities on each code commit with GitLab CI/CD. This scan relies on open source tools and on the integration with Gemnasium technology (now part of GitLab) to show, in-line with every merge request, vulnerable dependencies needing updating. Results are collected and available as a single report.
            link:
              href: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/
              text: Learn More
              data_ga_name: dependency scanning learn more
              data_ga_location: body
          - title: Licence Compliance
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Upon code commit, project dependencies are searched for approved and blacklisted licenses defined by custom policies per project. Software licenses being used are identified if they are not within policy. This scan relies on an open source tool, LicenseFinder and license analysis results are shown in-line for every merge request for immediate resolution.
            link:
              href: https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html
              text: Learn More
              data_ga_name: licence compliance learn more
              data_ga_location: body
          - title: Vulnerability Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              View, triage, trend, track, and resolve vulnerabilities detected in your applications.
            link:
              href: https://docs.gitlab.com/ee/user/application_security/security_dashboard/
              text: Learn More
              data_ga_name: vulnerability management learn more
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/secure/){data-ga-name="secure direction" data-ga-location="body"}.
          - header: 4 of the top 6 attacks were application based
            text: |
              Download our whitepaper, "A Seismic Shift in Application Security" to learn how to protect your organization.
            align_center: true
            link_href: /resources/whitepaper-seismic-shift-application-security/
            link_text: Download Whitepaper
            link_data_ga_name: application security whitepaper
            link_data_ga_location: body
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Create
            icon:
              name: create-alt-2
              alt: Create Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 500
            text: Create, view, manage code and project data through powerful branching tools.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/create/
              data_ga_name: create
              data_ga_location: body
          - title: Protect
            icon:
              name: protect-alt-2
              alt: Protect Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1000
            text: |
              Protect your apps and infrastructure from security intrusions.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/protect/
              data_ga_name: protect
              data_ga_location: body
          - title: Configure
            icon:
              name: configure-alt-2
              alt: Configure Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1500
            text: |
              Configure your applications and infrastructure.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/configure/
              data_ga_name: configure
              data_ga_location: body
